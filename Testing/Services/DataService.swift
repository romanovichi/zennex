//
//  DataService.swift
//  Testing
//
//  Created by Иван Романович on 07.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation

class DataService {
    
    private var repository = Repository<Company>()
}

extension DataService{
    
    func saveToRealm(_ object: Company) {
        repository.save(item: object)
    }
    
    func getFormRealm() -> Company {
        if let company = repository.getItemsFromDatabase().first {
            return company
        } else {
            let company = Company()
            saveToRealm(company)
        }
        return getFormRealm()
    }
    
}
