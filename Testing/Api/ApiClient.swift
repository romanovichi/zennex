//
//  ApiClient.swift
//  Testing
//
//  Created by Иван Романович on 06.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Kingfisher

class ApiClient {
    
    func getData(_ completion: @escaping (QuotesResponse) -> ()) {
        
        Alamofire.request(Constants.serviceUrlString, method: .get).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else { return }
                if let json = try? JSON(data: data) {
                    completion(QuotesResponse.from(json: json))
                }
            } else {
                print("Error in getData: \(String(describing: response.result.error))")
            }
        }
    }
}
