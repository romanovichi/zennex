//
//  Manager.swift
//  Testing
//
//  Created by Иван Романович on 02.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class Manager: Person {
    
    @objc dynamic var officeHours: String = ""
    
    convenience init(fullName: String, salary: Double, officeHours: String) {
        self.init()
        self.fullName = fullName
        self.salary = salary
        self.officeHours = officeHours
    }
}
