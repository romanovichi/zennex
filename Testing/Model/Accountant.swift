//
//  Accountant.swift
//  Testing
//
//  Created by Иван Романович on 02.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

enum AccountantRole: String {
    case salary = "Начисление зарплаты"
    case materials = "Учет материалов"
}

class Accountant: Person {
    
    @objc dynamic var workSpaceNumber: Int = 0
    @objc dynamic var lunchTime: String = ""
    @objc dynamic var _accountantRole: String = ""
    
    var accountantRole: AccountantRole?  {
        set {
            if let role = newValue {
                _accountantRole = role.rawValue
            }
        }
        get {
            if let role = AccountantRole(rawValue: _accountantRole) {
                return role
            }
            return nil
        }
    }
    
    convenience init(fullName: String, salary: Double, workSpaceNumber: Int, lunchTime: String, accountantRole: AccountantRole) {
        self.init()
        self.fullName = fullName
        self.salary = salary
        self.workSpaceNumber = workSpaceNumber
        self.accountantRole = accountantRole
        self.lunchTime = lunchTime
    }
}
