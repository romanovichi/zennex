//
//  Quotes.swift
//  Testing
//
//  Created by Иван Романович on 06.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyJSON

class Quote {
    
    var id: Int
    var date: String
    var rating: Double
    var description: String
    
    init(id: Int, date: String, rating: Double, description: String) {
        self.id = id
        self.date = date
        self.rating = rating
        self.description = description
    }
}
