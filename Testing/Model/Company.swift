//
//  Company.swift
//  Testing
//
//  Created by Иван Романович on 08.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class Company: Object {
    
    var managerList = List<Manager>()
    var employeeList = List<Employee>()
    var accountantList = List<Accountant>()
}
