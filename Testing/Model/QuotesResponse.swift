//
//  DataFromService.swift
//  Testing
//
//  Created by Иван Романович on 06.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyJSON

class QuotesResponse {
    
    var lastTime: String
    var quotes: [Quote]
    var total: Int
    
    init(lastTime: String, quotes: [Quote], total: Int) {
        self.lastTime = lastTime
        self.quotes = quotes
        self.total = total
    }
    
    static func from(json: JSON) -> QuotesResponse {
        
        let lastTime = json["last"].stringValue
        let total = json["total"].intValue
        let quotesJsonArray = json["quotes"].arrayValue
        var quotes = [Quote]()
        
        for item in quotesJsonArray {
            let rowDescription = item["description"].stringValue
            let description = rowDescription.removeHtmlTags

            let quote = Quote.init(id: item["id"].intValue, date: item["time"].stringValue, rating: item["rating"].doubleValue, description: description)
            quotes.append(quote)
        }
        
        let dataFromService = QuotesResponse(lastTime: lastTime, quotes: quotes, total: total)
        return dataFromService
    }
}
