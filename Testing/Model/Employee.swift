//
//  Employee.swift
//  Testing
//
//  Created by Иван Романович on 02.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class Employee: Person {
    
    @objc dynamic var workSpaceNumber: Int = 0
    @objc dynamic var lunchTime: String = ""
    
    convenience init(fullName: String, salary: Double, workSpaceNumber: Int, lunchTime: String) {
        self.init()
        self.fullName = fullName
        self.salary = salary
        self.workSpaceNumber = workSpaceNumber
        self.lunchTime = lunchTime
    }
}
