//
//  ServiceVC.swift
//  Testing
//
//  Created by Иван Романович on 01.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class ServiceVC: UIViewController {
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var lastDateLabel: UILabel!
    @IBOutlet weak var quotesLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    private let apiClient = ApiClient()
    private var quotesResponse: QuotesResponse?
    
    private var text: String = "" {
        didSet {
            quotesLabel.text = text
            activityIndicatorView.stopAnimating()
            activityIndicatorView.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView.startAnimating()
        activityIndicatorView.isHidden = false

        apiClient.getData { [weak self] (quotesResponse) in
            self?.quotesResponse = quotesResponse
            self?.totalLabel.text = String(quotesResponse.total)
            self?.lastDateLabel.text = quotesResponse.lastTime
            
            for quote in quotesResponse.quotes {
                self?.text += "ID: " + String(quote.id) + "\nДата: " + quote.date + "\nРейтинг: " + String(quote.rating) + "\nЦитата: \n\n" + quote.description + "\n\n\n"
                
            }
        }
    }
}
