//
//  GalleryVC.swift
//  Testing
//
//  Created by Иван Романович on 01.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class GalleryVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var toolBar: UIToolbar!
    
    private let apiClient = ApiClient()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        galleryCollectionView.register(UINib.init(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCell")
    }
}


extension GalleryVC {
    
    @IBAction private func nextPictureBurronPressed(_ sender: Any) {
        
        let currentIndexPath = galleryCollectionView.indexPathsForVisibleItems.first!
        let newIndexPath = IndexPath(item: currentIndexPath.row + 1, section: currentIndexPath.section)
        
        if newIndexPath.row < galleryCollectionView.numberOfItems(inSection: newIndexPath.section) {
            galleryCollectionView.scrollToItem(at: newIndexPath, at: .right, animated: true)
        }
    }
    
    @IBAction private func previousPivtureButtonPressed(_ sender: Any) {
        
        let currentIndexPath = galleryCollectionView.indexPathsForVisibleItems.first!
        let newIndexPath = IndexPath(item: currentIndexPath.row - 1, section: currentIndexPath.section)
        
        if newIndexPath.row >= 0 {
            galleryCollectionView.scrollToItem(at: newIndexPath, at: .right, animated: true)
        }
    }
}


extension GalleryVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.amountOfImagesInGallery
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = galleryCollectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as? GalleryCollectionViewCell {
            cell.imageView.kf.setImage(with: URL(string: "https://picsum.photos/2200/3300?image=\(indexPath.row)"),
                                        placeholder: nil,
                                        options: [.forceRefresh],
                                        progressBlock: nil,
                                        completionHandler: nil)
            return cell
        }
        return UICollectionViewCell()
    }
}


extension GalleryVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
