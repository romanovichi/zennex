//
//  GalleryCollectionViewCell.swift
//  Testing
//
//  Created by Иван Романович on 05.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        scrollView.delegate = self
        
        self.scrollView.maximumZoomScale = 6.0
        self.scrollView.minimumZoomScale = 1.0

        imageView.kf.indicatorType = .activity
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
