//
//  ViewController.swift
//  Testing
//
//  Created by Иван Романович on 01.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class ListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let sections = ["Руководство", "Сотрудники", "Бухгалтерия"]
    
    private var company = Company()
    private var dataService = DataService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        company = dataService.getFormRealm()
        
        setupTableView()
        setupNavigationItems()
    }
}


extension ListVC {
    
    private func setupTableView() {
        
        tableView.register(UINib.init(nibName: "EmployeeCell", bundle: nil), forCellReuseIdentifier: "EmployeeCell")
        tableView.register(UINib.init(nibName: "ManagerCell", bundle: nil), forCellReuseIdentifier: "ManagerCell")
        tableView.register(UINib.init(nibName: "AccountantCell", bundle: nil), forCellReuseIdentifier: "AccountantCell")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 85
        tableView.tableFooterView = UIView()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupNavigationItems() {
        
        let leftBarButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editBarButtonPressed))
        let addBarButton = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(addBarButtonPressed))
        
        navigationItem.leftBarButtonItem = leftBarButton
        navigationItem.rightBarButtonItem = addBarButton
    }
}


extension ListVC {
    
    @objc private func editBarButtonPressed() {
        tableView.isEditing = !tableView.isEditing
    }
    
    @objc private func addBarButtonPressed() {
        let personDetails = PersonDetailsVC()
        self.navigationController?.pushViewController(personDetails, animated: true)
        
        personDetails.onSave = { [weak self] person in
            self?.saveObject(person)
            self?.tableView.reloadData()
        }
    }
}


extension ListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return company.managerList.count
        }
        if section == 1 {
            return company.employeeList.count
        }
        if section == 2 {
            return company.accountantList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ManagerCell", for: indexPath) as? ManagerCell {
                cell.person = company.managerList[indexPath.row]
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeCell", for: indexPath) as? EmployeeCell {
                cell.person = company.employeeList[indexPath.row]
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "AccountantCell", for: indexPath) as? AccountantCell {
                cell.person = company.accountantList[indexPath.row]
                return cell
            }
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteObject(from: indexPath)
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        switch sourceIndexPath.section {
        case 1:
            let objectToMove = company.employeeList[sourceIndexPath.row]
            company.employeeList.removeWithRealmTransaction(at: sourceIndexPath.row)
            company.employeeList.insertWithRealmTransaction(objectToMove, at: destinationIndexPath.row)
        case 2:
            let objectToMove = company.accountantList[sourceIndexPath.row]
            company.accountantList.removeWithRealmTransaction(at: sourceIndexPath.row)
            company.accountantList.insertWithRealmTransaction(objectToMove, at: destinationIndexPath.row)
        default:
            let objectToMove = company.managerList[sourceIndexPath.row]
            company.managerList.removeWithRealmTransaction(at: sourceIndexPath.row)
            company.managerList.insertWithRealmTransaction(objectToMove, at: destinationIndexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            return sourceIndexPath;
        } else {
            return proposedDestinationIndexPath;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let personDetails = PersonDetailsVC()
        switch indexPath.section {
        case 1:
            personDetails.edit(person: company.employeeList[indexPath.row])
        case 2:
            personDetails.edit(person: company.accountantList[indexPath.row])
        default:
            personDetails.edit(person: company.managerList[indexPath.row])
        }
        self.navigationController?.pushViewController(personDetails, animated: true)
        
        personDetails.onSave = { [weak self] person in
            
            self?.deleteObject(from: indexPath)
            self?.saveObject(person)
            self?.tableView.reloadData()
        }
    }
}


extension ListVC {
    
    func saveObject(_ person: Person) {
        if let person = person as? Employee {
            company.employeeList.appendWithRealmTransaction(person)
        }
        if let person = person as? Manager {
            company.managerList.appendWithRealmTransaction(person)
        }
        if let person = person as? Accountant {
            company.accountantList.appendWithRealmTransaction(person)
        }
    }
    
    func deleteObject(from indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            let objectToDelete = company.employeeList[indexPath.row]
            company.employeeList.deleteWithRealmTransaction(object: objectToDelete)
        case 2:
            let objectToDelete = company.accountantList[indexPath.row]
            company.accountantList.deleteWithRealmTransaction(object: objectToDelete)
        default:
            let objectToDelete = company.managerList[indexPath.row]
            company.managerList.deleteWithRealmTransaction(object: objectToDelete)
        }
    }
}

