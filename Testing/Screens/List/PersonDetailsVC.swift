//
//  PersonDetailsVC.swift
//  Testing
//
//  Created by Иван Романович on 03.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class PersonDetailsVC: UIViewController {

    @IBOutlet weak var officeHoursView: UIView!
    @IBOutlet weak var workSpaceView: UIView!
    @IBOutlet weak var lunchTimeView: UIView!
    @IBOutlet weak var accountantTypeView: UIView!
    
    @IBOutlet weak var personTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var accountantRoleSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var salaryTextTextField: UITextField!
    @IBOutlet weak var workSpaceTextField: UITextField!
    @IBOutlet weak var lunchTimeTextField: UITextField!
    @IBOutlet weak var officeHoursTextField: UITextField!
    
    var onSave: ((Person) -> ())?
    private var person: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationItem()
        setupFieldsForEditingPerson()
        hideKeyboardWhenTappedAround()
    }
}


extension PersonDetailsVC {
    
    func setupNavigationItem() {
        let save = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonPressed))
        navigationItem.rightBarButtonItem = save
    }
    
    func edit(person: Person) {
        self.person = person
    }
    
    private func setupFieldsForEditingPerson() {
        if let person = person as? Manager {
            personTypeSegmentedControl.selectedSegmentIndex = 0
            fullNameTextField.text = person.fullName
            salaryTextTextField.text = String(person.salary)
            officeHoursTextField.text = person.officeHours
        }
        if let person = person as? Employee {
            personTypeSegmentedControl.selectedSegmentIndex = 1
            fullNameTextField.text = person.fullName
            salaryTextTextField.text = String(person.salary)
            workSpaceTextField.text = String(person.workSpaceNumber)
            lunchTimeTextField.text = person.lunchTime
        }
        if let person = person as? Accountant {
            personTypeSegmentedControl.selectedSegmentIndex = 2
            fullNameTextField.text = person.fullName
            salaryTextTextField.text = String(person.salary)
            workSpaceTextField.text = String(person.workSpaceNumber)
            lunchTimeTextField.text = person.lunchTime
            
            if person.accountantRole == .salary {
                accountantRoleSegmentedControl.selectedSegmentIndex = 0
            } else {
                accountantRoleSegmentedControl.selectedSegmentIndex = 1
            }
        }
        personTypeSegmentedControlChanged(personTypeSegmentedControl)
    }
}


extension PersonDetailsVC{
    
    @objc private func saveButtonPressed() {
        guard fullNameTextField.text != "" else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        switch personTypeSegmentedControl.selectedSegmentIndex {
        case 1:
            if let fullName = fullNameTextField.text,
                let salary = salaryTextTextField.text,
                let workSpaceNumber = workSpaceTextField.text,
                let lunchTime = lunchTimeTextField.text {
                
                let newEmployee = Employee(fullName: fullName,
                                           salary: Double(salary) ?? 0,
                                           workSpaceNumber: Int(workSpaceNumber) ?? 0,
                                           lunchTime: lunchTime)
                onSave!(newEmployee)
                self.navigationController?.popViewController(animated: true)
            }
        case 2:
            if let fullName = fullNameTextField.text,
                let salary = salaryTextTextField.text,
                let workSpaceNumber = workSpaceTextField.text,
                let lunchTime = lunchTimeTextField.text {
                
                var accountantRole = AccountantRole.salary
                switch accountantRoleSegmentedControl.selectedSegmentIndex {
                case 1:
                    accountantRole = .materials
                default:
                    accountantRole = .salary
                }
               
                let newAccountant = Accountant(fullName: fullName,
                                               salary: Double(salary) ?? 0,
                                               workSpaceNumber: Int(workSpaceNumber) ?? 0,
                                               lunchTime: lunchTime,
                                               accountantRole: accountantRole)
                onSave!(newAccountant)
                self.navigationController?.popViewController(animated: true)
            }
        default:
            if let fullName = fullNameTextField.text,
                let salary = salaryTextTextField.text,
                let officeHours = officeHoursTextField.text {
                
                let newManager = Manager(fullName: fullName, salary: Double(salary) ?? 0, officeHours: officeHours)
                onSave!(newManager)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
   
    @IBAction private func personTypeSegmentedControlChanged(_ sender: Any) {
        if let segmentedControl = sender as? UISegmentedControl {
            if segmentedControl.selectedSegmentIndex == 0 {
                officeHoursView.isHidden = false
                workSpaceView.isHidden = true
                lunchTimeView.isHidden = true
                accountantTypeView.isHidden = true
            }
            if segmentedControl.selectedSegmentIndex == 1 {
                officeHoursView.isHidden = true
                workSpaceView.isHidden = false
                lunchTimeView.isHidden = false
                accountantTypeView.isHidden = true
            }
            if segmentedControl.selectedSegmentIndex == 2 {
                officeHoursView.isHidden = true
                workSpaceView.isHidden = false
                lunchTimeView.isHidden = false
                accountantTypeView.isHidden = false
            }
        }
    }
}
