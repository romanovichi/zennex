//
//  MainTapBarController.swift
//  Testing
//
//  Created by Иван Романович on 01.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.tintColor = .purple
        
        viewControllers = [
            generateNavigationController(for: ListVC(), title: "List", image: #imageLiteral(resourceName: "account")),
            generateNavigationController(for: GalleryVC(), title: "Gallery", image: #imageLiteral(resourceName: "trending")),
            generateNavigationController(for: ServiceVC(), title: "Service", image: #imageLiteral(resourceName: "home"))
        ]
    }
    
    fileprivate func generateNavigationController(for rootViewController: UIViewController, title: String, image: UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        rootViewController.navigationItem.title = title
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        return navController
    }
}

