//
//  BaseCell.swift
//  Testing
//
//  Created by Иван Романович on 02.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.


import UIKit

class BaseCell: UITableViewCell {
    
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconImageView.image = UIImage(named: "account")?.withRenderingMode(.alwaysTemplate)
        iconImageView.tintColor = #colorLiteral(red: 0.2319480011, green: 0.2511351695, blue: 0.2790093591, alpha: 1)
    }
    
    var person: Person? {
        didSet {
            if let person = person {
                employeeNameLabel.text = person.fullName
                salaryLabel.text = String(person.salary)
            }
        }
    }
    
    func set(person: Person) {
        self.person = person
    }
}
