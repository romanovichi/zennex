//
//  AccountantCell.swift
//  Testing
//
//  Created by Иван Романович on 03.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class AccountantCell: BaseCell {

    @IBOutlet weak var workSpaceNumberLabel: UILabel!
    @IBOutlet weak var accountantTypeLabel: UILabel!
    @IBOutlet weak var lunchTimeLabel: UILabel!
    
    override var person: Person? {
        didSet {
            if let person = person as? Accountant {
                workSpaceNumberLabel.text = String(person.workSpaceNumber)
                lunchTimeLabel.text = person.lunchTime
                if let role = person.accountantRole {
                    accountantTypeLabel.text = role.rawValue
                }
            }
        }
    }
}
