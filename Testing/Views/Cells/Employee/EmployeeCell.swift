//
//  EmployeeCell.swift
//  Testing
//
//  Created by Иван Романович on 01.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class EmployeeCell: BaseCell {
    
    @IBOutlet weak var workSpaceNumberLabel: UILabel!
    @IBOutlet weak var lunchTimeLabel: UILabel!
    
    override var person: Person? {
        didSet {
            if let person = person as? Employee {
                lunchTimeLabel.text = person.lunchTime
                workSpaceNumberLabel.text = String(person.workSpaceNumber)
            }
        }
    }
}
