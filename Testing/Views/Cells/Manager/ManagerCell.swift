//
//  ManagerCell.swift
//  Testing
//
//  Created by Иван Романович on 02.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit

class ManagerCell: BaseCell {

    @IBOutlet weak var officeHoursLabel: UILabel!
    
    override var person: Person? {
        didSet {
            if let person = person as? Manager {
                officeHoursLabel.text = person.officeHours
            }
        }
    }
}
