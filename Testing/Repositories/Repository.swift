//
//  Repository.swift
//  Testing
//
//  Created by Иван Романович on 07.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class Repository<T> where T: Company {
    
    let realm = try! Realm()
    
    func save(item: T) {
        try? realm.write {
            realm.add(item)
        }
    }
    
    func deleteAll(){
        try? realm.write {
            realm.deleteAll()
        }
    }
    
    func getItemsFromDatabase() -> [T] {
        return realm.objects(T.self).map { $0 }
    }
}

