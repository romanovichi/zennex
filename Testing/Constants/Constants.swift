//
//  Constants.swift
//  Testing
//
//  Created by Иван Романович on 07.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation

struct Constants {
    static let amountOfImagesInGallery = 15
    static let serviceUrlString = "http://quotes.zennex.ru/api/v3/bash/quotes?sort=time"
}
