//
//  List.swift
//  Testing
//
//  Created by Иван Романович on 08.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

extension List {
    
    func appendWithRealmTransaction(_ object: Element) {
        
        let realm = try! Realm()
        
        try? realm.write {
            self.append(object)
        }
    }
    
    func deleteWithRealmTransaction(object: Object) {
        
        let realm = try! Realm()
        
        try? realm.write {
            realm.delete(object)
        }
    }
    
    func removeWithRealmTransaction(at index: Int) {
        
        let realm = try! Realm()
        
        try? realm.write {
            self.remove(at: index)
        }
    }
    
    func insertWithRealmTransaction(_ object: Element, at index: Int) {
        
        let realm = try! Realm()
        
        try? realm.write {
            self.insert(object, at: index)
        }
    }
    
}

