//
//  String.swift
//  Testing
//
//  Created by Иван Романович on 06.02.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation

extension String {
    
    var removeHtmlTags: String {
        var newString = self.replacingOccurrences(of: "<br>", with: "\n", options: .regularExpression, range: nil)
        newString = newString.replacingOccurrences(of: "&quot;", with: "\"", options: .regularExpression, range: nil)
        newString = newString.replacingOccurrences(of: "&lt;", with: "<", options: .regularExpression, range: nil)
        newString = newString.replacingOccurrences(of: "&gt;", with: ">", options: .regularExpression, range: nil)
        newString = newString.replacingOccurrences(of: "<([a-z][a-z0-9]*)\\b[^>]*>(.*?)</\\1>", with: "", options: .regularExpression, range: nil)

        return newString
    }
    
}
